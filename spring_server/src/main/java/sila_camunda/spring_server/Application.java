package sila_camunda.spring_server;

import org.camunda.bpm.spring.boot.starter.annotation.EnableProcessApplication;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import sila_camunda.core.StartController;

@SpringBootApplication
@EnableProcessApplication
public class Application {
    public static void main(String... args) {
        StartController.init();
        SpringApplication.run(Application.class, args);
    }
}
