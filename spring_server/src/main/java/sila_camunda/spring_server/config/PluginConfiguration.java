package sila_camunda.spring_server.config;

import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import sila_camunda.core.tasks.SiLASynchronousCall;

/**
 * Camunda Bean Generation
 */
@Configuration
public class PluginConfiguration {

    @Bean
    public static JavaDelegate siLASynchronousCall() {
        return new SiLASynchronousCall();
    }

    // Used for Engine Plugins in the future
    /*
    @Bean
    public static ProcessEnginePlugin siLAProcessEnginePlugin() {
        return new SiLAProcessEnginePlugin();
    }
    */
}
