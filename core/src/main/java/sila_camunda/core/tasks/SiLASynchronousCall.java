package sila_camunda.core.tasks;

import com.google.common.collect.Sets;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import io.grpc.StatusRuntimeException;
import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import sila_java.library.manager.ServerFinder;
import sila_java.library.manager.ServerManager;
import sila_java.library.manager.models.Server;
import sila_java.library.manager.models.SiLACall;

import java.security.KeyException;
import java.util.List;
import java.util.Set;

/**
 * Synchronous Service Task to Call Commands or Get Properties
 *
 * @implNote Input and Output Mappings are created according to the protobuf specification.
 */
@Slf4j
public class SiLASynchronousCall implements JavaDelegate {
    public static final String serverKey = "serverName";
    public static final String featureKey = "featureIdentifier";
    public static final String callKey = "callIdentifier";
    public static final String callTypeKey = "callType";
    public static final String callParameterKey = "callParameter";
    public static final String responseKey = "responseReference";
    private static final Set<String> callKeySet = Sets.newHashSet(
            serverKey, featureKey, callKey, callTypeKey, callParameterKey, responseKey
    );

    @Override
    public void execute(DelegateExecution execution) {
        try {
            callKeySet.forEach(
                    (callKey)->{
                        if (!execution.getVariables().containsKey(callKey)) {
                            throw new SiLAError(callKey +
                                    " needs to be defined in task: " +
                                    execution.getActivityInstanceId()
                            );
                        }
                    }
            );

            final String serverName = String.valueOf(execution.getVariable(serverKey));
            final String featureIdentifier = String.valueOf(execution.getVariable(featureKey));
            final String callIdentifier = String.valueOf(execution.getVariable(callKey));
            final SiLACall.Type callType = SiLACall.Type.valueOf(
                    String.valueOf(execution.getVariable(callTypeKey))
            );
            final String callParameter = String.valueOf(execution.getVariable(callParameterKey));
            final String responseReference = String.valueOf(execution.getVariable(responseKey));

            final List<Server> serverList = ServerFinder.filterBy(ServerFinder.Filter.name(serverName)).find();

            if (serverList.size() == 0) {
                throw new SiLAError("Server with Name " + serverName + " not found!");
            }

            if (serverList.size() > 1) {
                throw new SiLAError("There are at least 2 Serves with Name " + serverName
                        + " has to be unique!");
            }

            final Server server = serverList.get(0);

            log.info("Calling " + serverName + " - " +
                    featureIdentifier + " - " +
                    callIdentifier
            );
            log.info("Payload found: " + callParameter);

            try {
                final String response = ServerManager.getInstance().newCallExecutor(
                        new SiLACall(
                                server.getConfiguration().getUuid(),
                                featureIdentifier,
                                callIdentifier,
                                callType,
                                callParameter
                        )
                ).execute();

                final JsonObject responseValue = new JsonParser().parse(response)
                        .getAsJsonObject();

                final String responseContent = responseValue.toString();
                execution.setVariable(responseReference, responseContent);
                log.info("Set " + responseReference + " to value "
                        + responseContent);
            } catch (KeyException e) {
                throw new SiLAError(e.getMessage());
            } catch (StatusRuntimeException e) {
                final Throwable cause = e.getCause();

                final String errorMessage;
                if (cause != null) {
                    errorMessage = cause.getMessage();
                } else {
                    errorMessage = e.getMessage();
                }

                throw new SiLAError(errorMessage);
            }
        } catch (SiLAError e) {
            log.error(e.getMessage());
            throw e;
        }
    }
}
