package sila_camunda.core.tasks;

import org.camunda.bpm.engine.delegate.BpmnError;

public class SiLAError extends BpmnError {
    public SiLAError(String message) {
        super("ProcessError", message);
    }
}
