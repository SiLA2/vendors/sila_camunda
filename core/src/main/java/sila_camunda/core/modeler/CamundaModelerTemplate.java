package sila_camunda.core.modeler;

import com.google.gson.Gson;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import sila_camunda.core.tasks.SiLASynchronousCall;
import sila_java.library.core.models.Feature;
import sila_java.library.manager.models.Server;
import sila_java.library.manager.models.SiLACall;

import java.util.ArrayList;
import java.util.List;

/**
 * Parses a list of SiLA2 Server Features into a Camunda Modeler template structure. The structure can be exported as
 * JSON and i placed into the camunda modeler directory <camunda-modeler>/resources/element-templates
 *
 * @author Sebastian Schaerer
 */
@Slf4j
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public abstract class CamundaModelerTemplate {
    private static final Property IMPLEMENTATION_PROPERTY = new Property(
            "Implementation Expression",
            "Hidden",
            "${siLASynchronousCall}",
            false,
            new Binding(
                    "property",
                    "camunda:delegateExpression",
                    null
            ),
            "Base Synchronous Call Implementation"
    );

    private static final Property ASYNC_PROPERTY = new Property(
            "Asynchronous Save",
            "Hidden",
            String.valueOf(true),
            false,
            new Binding(
                    "property",
                    "camunda:asyncBefore",
                    null
            ),
            "Always synchronous for saver stepping."
    );

    /**
     * Create a Modeler Template for all Commands and Properties related to a SiLA Server
     * @param server SiLA Server Information
     * @return Json Array of Service Task Templates
     */
    public static List<Model> createModelerTemplate(@NonNull Server server) {
        final List<Model> modelBindings = new ArrayList<>();

        server.getFeatures().forEach(
                (feature -> {
                    feature.getCommand().forEach(
                            command -> {
                                final List<Property> properties = new ArrayList<>();

                                final SiLACall.Type callType =
                                        command.getObservable().equals("Yes") ?
                                                SiLACall.Type.OBSERVABLE_COMMAND :
                                                SiLACall.Type.UNOBSERVABLE_COMMAND;

                                properties.addAll(
                                        getSiLACallIdentifierProperties(
                                                server, feature,
                                                command.getIdentifier(),
                                                command.getDescription(),
                                                callType
                                        )
                                );

                                // Only Commands have Call Parameters
                                properties.add(new Property(
                                        "Call Parameter",
                                        "Text",
                                        "",
                                        true,
                                        new Binding(
                                                "camunda:inputParameter",
                                                SiLASynchronousCall.callParameterKey,
                                                "javascript"
                                        ),
                                        "Parameter for Command in protobuf JSON Format: " + new Gson().toJson(command.getParameter())
                                ));


                                final String callId = server.getInformation().getType() + "." +
                                        feature.getIdentifier() + command.getIdentifier();
                                modelBindings.add(
                                        new Model(callId, callId, properties)
                                );
                            }
                    );

                    feature.getProperty().forEach(
                            property -> {
                                final List<Property> properties = new ArrayList<>();

                                final SiLACall.Type callType =
                                        property.getObservable().equals("Yes") ?
                                                SiLACall.Type.OBSERVABLE_PROPERTY :
                                                SiLACall.Type.UNOBSERVABLE_PROPERTY;

                                properties.addAll(
                                        getSiLACallIdentifierProperties(
                                                server, feature,
                                                property.getIdentifier(),
                                                property.getDescription(),
                                                callType
                                        )
                                );

                                // Only Commands have Call Parameters
                                properties.add(new Property(
                                        "Call Parameter",
                                        "String",
                                        "{}",
                                        false,
                                        new Binding(
                                                "camunda:inputParameter",
                                                SiLASynchronousCall.callParameterKey,
                                                null
                                        ),
                                        "Properties don't need parameters"
                                ));


                                final String callId = server.getInformation().getType() + "." +
                                        feature.getIdentifier() + property.getIdentifier();
                                modelBindings.add(
                                        new Model(callId, callId, properties)
                                );
                            }
                    );
                })
        );

        return modelBindings;
    }

    /**
     * Common Camunda Properties for Commands and SiLA Properties
     */
    private static List<Property> getSiLACallIdentifierProperties(
            @NonNull Server server,
            @NonNull Feature feature,
            @NonNull String callIdentifier,
            @NonNull String callDescription,
            @NonNull SiLACall.Type callType) {
        final List<Property> properties = new ArrayList<>();
        properties.add(IMPLEMENTATION_PROPERTY);
        properties.add(ASYNC_PROPERTY);

        properties.add(new Property(
                "Server Name",
                "String",
                server.getConfiguration().getName(),
                false,
                new Binding(
                        "camunda:inputParameter",
                        SiLASynchronousCall.serverKey,
                        null
                ),
                server.getInformation().getDescription()
        ));
        properties.add(new Property(
                "Feature Identifier",
                "String",
                feature.getIdentifier(),
                false,
                new Binding(
                        "camunda:inputParameter",
                        SiLASynchronousCall.featureKey,
                        null
                ),
                feature.getDescription()
        ));
        properties.add(new Property(
                "Call Identifier (Command or Property)",
                "String",
                callIdentifier,
                false,
                new Binding(
                        "camunda:inputParameter",
                        SiLASynchronousCall.callKey,
                        null
                ),
                callDescription
        ));
        properties.add(new Property(
                "Call Type",
                "String",
                callType.toString(),
                false,
                new Binding(
                        "camunda:inputParameter",
                        SiLASynchronousCall.callTypeKey,
                        null
                ),
                "Observable/Unobservable Command or Property"
        ));

        properties.add(new Property(
                "Response Reference",
                "String",
                "",
                true,
                new Binding(
                        "camunda:inputParameter",
                        SiLASynchronousCall.responseKey,
                        null
                ),
                "Name of Context Variable and how its saved"
        ));

        return properties;
    }
}
