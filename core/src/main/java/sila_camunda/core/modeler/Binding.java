package sila_camunda.core.modeler;

import lombok.Value;

@Value
public class Binding {
    private String type;
    private String name;
    private String scriptFormat;
}
