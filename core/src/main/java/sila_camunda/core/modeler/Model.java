package sila_camunda.core.modeler;

import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

@Getter
public class Model {
    private String name;
    private String id;
    private String[] appliesTo = {"bpmn:ServiceTask"};
    private List<Property> properties = new ArrayList<>();

    public Model(String name, String id, List<Property> properties) {
        this.name = name;
        this.id = id;
        this.properties.addAll(properties);
    }
}
