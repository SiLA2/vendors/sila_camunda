package sila_camunda.core.modeler;

import lombok.Value;

import java.util.ArrayList;
import java.util.List;

@Value
public class Property {
    private String label;
    private String type;
    private String value;
    private boolean editable;
    private Binding binding;
    private String description;
    private List<Choice> choices = new ArrayList<>();
    private Constraint constraints = new Constraint();
}
