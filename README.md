# SiLA Camunda

This repository contains implementations, allowing  Camunda BPM to discover, connect and call SiLA Servers using the
Camudna BPM Engine.

For more general information about the standard, we refer to the [sila_base](https://gitlab.com/SiLA2/sila_base) repository.

## License
This code is licensed under the [MIT License](https://en.wikipedia.org/wiki/MIT_License)

## Maintainer
This repository is maintained by:

 * Sebastian Schaerer ([sebastian.schaerer@wega-it.com](mailto:sebastian.schaerer@wega-it.com)) of [wega Informatik AG](http://www.wega-it.com)
 * Maximilian Schulz ([max@unitelabs.ch](mailto:max@unitelabs.ch)) of [UniteLabs](http://www.unitelabs.ch)

## Components
### core
Core SiLA Bindings for Camunda to be used in plugins and server.

### cockpit_plugin
Simple Camunda Cockpit Plugin to display the SiLA Servers found in the Cockpit Dashboard. To be installed as library of
the Camunda Web Application.

### spring_server
Standalone Application with the Camunda engine and SiLA Bindings embedded.

## Installation
The code has been tested with JRE 1.8.0 / Oracle JDK 8 / Camunda BPM 7.9.0 on Spring with an H2 database.

## How to use it?
Have a look at the README file in `spring_server`.