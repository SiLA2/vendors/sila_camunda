package sila_camunda.cockpit_plugin.dto;

import lombok.Data;

@Data
public class AddServerBody {
    private String host;
    private int port;
}
