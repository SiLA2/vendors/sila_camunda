package sila_camunda.cockpit_plugin;

import java.util.*;

import sila_camunda.cockpit_plugin.resources.CockpitPluginRootResource;
import org.camunda.bpm.cockpit.plugin.spi.impl.AbstractCockpitPlugin;

public class CockpitPlugin extends AbstractCockpitPlugin {
    public static final String ID = "cockpit-plugin";

    public String getId() {
      return ID;
    }

    @Override
    public Set<Class<?>> getResourceClasses() {
        final Set<Class<?>> classes = new HashSet<>();
        classes.add(CockpitPluginRootResource.class);
        return classes;
    }
}
